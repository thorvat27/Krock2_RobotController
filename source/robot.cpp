#include <iostream>
#include <fstream>
#include <cmath>

#include "robot.hpp"


using namespace std;


extern double get_timestamp();

std::mutex dx_write, dx_read;


void 
robotComm(Robot *robot, int numMotors, int *used_ids)
{
    double t1, t2, t3, dt;
    double fbck_angles[MAX_NUM_SERVOS];
    double fbck_current[MAX_NUM_SERVOS];
    double angles[MAX_NUM_SERVOS];

    vector<int> position_ctrl_servos(NUM_MOTORS);
    for(int i=0; i<numMotors; i++){
        position_ctrl_servos[i]=USED_IDS[i];
    }
    //static ofstream mpcTimeLog("./data/mpcTimeLog.txt");
    while(!robot->stopThread){
        t1=get_timestamp();




        // READ FEEDBACK
        if(robot->readPosition){
            robot->getAllPositions(fbck_angles);
        }
        if(robot->readCurrent){
            //robot->getAllCurrents(fbck_current);
        }
        
        dx_read.lock();
            for(int i=0; i<numMotors; i++){
                robot->fbck_angles[i] = fbck_angles[i];
                robot->fbck_current[i] = fbck_current[i];
            }
        dx_read.unlock();

        //usleep(500);

        // WRITE NEW ANGLES
        dx_write.lock();
            for(int i=0; i<numMotors; i++){
                angles[i] = robot->angles[i];
            }
        dx_write.unlock();

        if(robot->writeAngles){
            robot->setAllPositions(angles, robot->usedIDs);
        }





        t2=get_timestamp();

        // SLEEP
        if(0.01 > (t2-t1)){
            usleep((0.01 - (t2-t1))*1000000);
        }

        t3=get_timestamp();
        //for(int i=0; i<10; i++){
        //    cout << angles[i] << endl;
        //}
        //cout << "Robot thread time: " << (t2-t1)*1000 <<" ms"<<endl;

    }   
}




/* Robot constructor // : d(0,1, portName)*/
Robot::Robot()
{
    readPosition=false;
    readCurrent=false;
    writeAngles=false;
    stopThread=false;

}

void
Robot::initSharedMemory()
{
    rStatus.WriteSharedMemory(); printf("written to a shm\n");
    sensorsStruct_shm = ssshm.find_or_construct<SensorsStruct>("SensorsStruct")();
}

bool
Robot::initMotorSystem(int speed, int torque_in, int numMotorsConfig, int used_ids[30], int enabled_ids[30], const char *portName)
{
    d = *dxsystemSetup(1, portName);
    cout << "Creating dynamixel system"<<endl;
    numMotors = d.getNumMotors();
    cout << "numMotors = "<<numMotors << endl;
    for(int i=0; i<numMotors; i++){
        cout<<"model: "<<d.getModel(i)<<endl;
    }

    if(numMotors!=numMotorsConfig){
        cout << "WRONG NUMBER OF MOTORS IN THE CONFIG FILE." << endl;
        numMotors = numMotorsConfig;
        return false;
    }


    ServosOn(enabled_ids);

    /* USED MOTORS */
    for(int i=0; i<numMotors; i++){

        ids_arr[i]=enabled_ids[i];
        cout << i << " ENABLED IDS " << ids_arr[i] << endl;
        if(enabled_ids[i]){
            usedIDs.push_back(used_ids[i]);
        }
        
    }

    /* Initialize center values of servos */
    for (int i = 0; i < numMotors; i++)
    {
        d_center[i] = 0;
    }

    //d_center[numMotors-16+2] = -90 * M_PI / 180.0;
    //d_center[numMotors-16+2+4] = 90 * M_PI / 180.0;
    //d_center[numMotors-16+2+8] = -90 * M_PI / 180.0;
    //d_center[numMotors-16+2+12] = 90 * M_PI / 180.0;

    setAllDelay(0);

    usleep(10000);
    // set servos max speed 
    MaxSpeed(speed);
    usleep(400000);
    // set servos max torque 
    TorqueLimit(torque_in);

    usleep(400000);
    InitialPosture(ids_arr);
    usleep(400000);

 
    
    int used_ids_thread[30];
    for(int i=0; i<30; i++){
        used_ids_thread[i] = used_ids[i];
    }
    robot_comm_thread=std::thread(robotComm, this, numMotors, used_ids_thread);  //robotComm(Robot *robot, int numMotors, int *used_ids)
    robot_comm_thread.detach();

    return true;
}

Robot::~Robot()
{
}

void 
Robot::readIMU(double *imudata)
{   
    memcpy(&sensorsStruct, sensorsStruct_shm, sizeof(SensorsStruct));
    imudata[0]=sensorsStruct.imu[0]*M_PI/180.;
    imudata[1]=sensorsStruct.imu[1]*M_PI/180.;
    imudata[2]=sensorsStruct.imu[2]*M_PI/180.;
}

void 
Robot::readForceSensors(double *forcedata, double *forcedataRaw)
{   
    memcpy(&sensorsStruct, sensorsStruct_shm, sizeof(SensorsStruct));
    for(int i=0; i<12; i++){
        forcedata[i]=sensorsStruct.optoforce[i];
    }
    for(int i=0; i<16; i++){
        forcedataRaw[i]=sensorsStruct.optoforceRaw[i];
    }
}

void 
Robot::writeRobotStatus(MatrixXd joint_angles, MatrixXd fbck_torques, MatrixXd forRPY, MatrixXd force)
{
    for(int i=0; i<NUM_MOTORS; i++){
        rStatus.robotStatusStruct.motor_positions[i]=joint_angles(i);
        rStatus.robotStatusStruct.motor_torques[i]=fbck_torques(i);
    }
    for(int i=0; i<2; i++){
        rStatus.robotStatusStruct.imu[i]=forRPY(i);
    }
    for(int i=0; i<12; i++){
        rStatus.robotStatusStruct.forces[i]=force(i);    
    }
    rStatus.WriteSharedMemory();
}

/* Sets the position of one servo to an angle in radians */
void
Robot::setSinglePosition(int id, double angle)
{
    d.setPosition(id, angle);
}

/* Sets the torque of one servo*/
void
Robot::setSingleTorque(int id, int torque)
{
    //d_cm730->WriteWord(id, TORQUE_LIMIT_L, torque,0);
}


/* read individual servo position */
int
Robot::getSinglePosition(int id)
{
     return(d.getPosition(id));
}


/* Sets the position of all servos to a table of angles in radians */
void
Robot::setAllPositions(double *angles, const vector<int>& ids)
{
    vector<int> goalPositions;

    for(unsigned int i=0; i<ids.size(); i++){
        if(ids_arr[i]){
            goalPositions.push_back(angle2Position(angles[i], d.getModel(i), IS_RADIAN));
        }
        //cout << i<<"\t" <<ids[i]<<"\t" <<ids_arr[i]<< "\t" <<  goalPositions[i] <<"\t" << angles[ids[i]]<< endl;
    }

    if(DISABLE_ALL_MOTORS==0){
        d.setIDsPosition(ids, goalPositions);
    }
    
}

/* Sets the position of all servos to a table of angles in radians */
void
Robot::setAllTorques(double *torques, const vector<int>& ids)
{
    vector<int> goalTorques;
    for(unsigned int i=0; i<ids.size(); i++){
        if(ids_arr[i])
            goalTorques.push_back(translateTorque(torques[i]));
    }
    d.setIDsTorques(ids, goalTorques);
}

/* Gets the position of all servos to a table of angles in radians */
void
Robot::getAllPositions(double *angles)
{
    vector<int> positions = d.getAllPosition();
    for(int i=0; i<numMotors; i++){
        if(ids_arr[i])
            angles[i]=position2Angle(positions[i], d.getModel(i));
    }

}


/* Gets the position of all servos to a table of angles in radians */
void
Robot::getIDsPositions(double *angles, const vector<int>& usedIDs)
{
    vector<int> positions = d.getIDsPosition(usedIDs);
    for(int i=0; i<numMotors; i++){
        if(ids_arr[i])
            angles[i]=position2Angle(positions[i], d.getModel(i));
    }

}

/* Sets the position of all servos to center */
void
Robot::InitialPosture(int *ids)
{
    //MovingSpeed(50, ids);
    //MaxTorque(300, ids);
    setAllPositions(d_center, usedIDs);
    usleep(2 * MILLION);
}

/* Sets the maximum torque of all servos to a specific value */
void
Robot::MaxTorque(int torque)
{
    vector<int> torque_vec (numMotors,torque);
    d.setAllTorqueMax(torque_vec);
}

/* Sets the maximum torque of all servos to a specific value */
void
Robot::TorqueLimit(int torque)
{
    vector<int> torque_vec (numMotors,torque);
    d.setAllTorqueLimit(torque_vec);
}
/* Sets the moving speed of all servos to a specific value */
void
Robot::MaxSpeed(int speed)
{
    vector<int> goalSpeeds (numMotors,speed);
    d.setAllSpeed(goalSpeeds);
}

/* Sets up torque mode */
void
Robot::TorqueMode(int *table, int *ids)
{
//  SyncWriteByteTable(TORQUE_MODE_ENABLE, table, ids);
}


/* Turn servo power on */
void
Robot::ServosOn(int *ids)
{
    for(int i=0; i<numMotors; i++){
        if(ids[i]){
            d.queueByte(i, P_TORQUE_ENABLE, 1);
        }
        else{
            d.queueByte(i, P_TORQUE_ENABLE, 0);
        }
    }
    d.executeQueue();
}

/* Turn servo power off */
void
Robot::ServosOff(int *ids)
{
    for(int i=0; i<numMotors; i++){
        if(ids[i]){
            d.queueByte(i, P_TORQUE_ENABLE, 0);
        }
    }
    d.executeQueue();
}


/* read individual servo torque enable */
void
Robot::getTorqueEnable(){
    for(int i=0; i<numMotors; i++){
        cout << "torque_en_" <<i<<": "<<d.getTorqueEn(i) << "\t";
    }
    cout << endl;
}


/* Set retrun delay time of motors */
void
Robot::setAllDelay(int val)
{
    d.setAllDelay(val);
}

/* Sets the P gain of all servos*/
void
Robot::setAllGainP(int Pgain)
{
    vector<int> Pgains_vec;
    for(int i=0; i<numMotors; i++){
        if(ids_arr[i])
            Pgains_vec.push_back(Pgain);
    }
    d.setAllGainP(Pgains_vec);
}



void
Robot::setTorqueControl(int *torque_ctrl_on, const vector<int>& ids)
{
    vector<int> trq_ctrl;
    for(unsigned int i=0; i<ids.size(); i++){
        if(ids_arr[i])
            trq_ctrl.push_back(torque_ctrl_on[i]);
    }
    d.setIDsTorqueControl(ids, trq_ctrl);
}


int 
Robot::translateTorque(double torque){
    int tornum=0;
    if(torque>1)
        torque=1;
    if(torque<-1){
        torque=-1;
    }
    if(torque>=0){
        tornum=(int)(torque*1023);
    }
    else{
        tornum=(int)(1023 + 1023*(-torque));
    }
    return tornum;
}

/* Gets the position of all servos to a table of angles in radians */
void
Robot::getAllCurrents(double *currents)
{
    vector<int> current_vec = d.getAllCurrent();
    for(int i=0; i<numMotors; i++){
        
        if(ids_arr[i]){
            currents[i]=(current_vec[i]-2048)*9.2115/2048;
            //cout << current_vec[i] << endl;
        }
    }
}


void 
Robot :: setAllPositionsThread(double* new_angles)
{
    dx_write.lock();
        for(int i=0; i<numMotors; i++){
            angles[i] = new_angles[i];
        }
        writeAngles=true;
    dx_write.unlock();
}

void 
Robot :: getAllPositionsThread(double *angles)
{
    dx_read.lock();
        for(int i=0; i<numMotors; i++){
            angles[i] = fbck_angles[i];
        }
        readPosition=true;
    dx_read.unlock();
}

void 
Robot :: getAllCurrentsThread(double *currents)
{
    dx_read.lock();
        for(int i=0; i<numMotors; i++){
            currents[i] = fbck_current[i];
        }
        readCurrent=true;
    dx_read.unlock();
}