#include "mlp.hpp" 
#include "utils.hpp"

/*  Constructor */
MLP_feedforward :: MLP_feedforward()
{
}

/*  Constructor */
MLP_feedforward :: MLP_feedforward(int inputSize, int hiddenLayerSize, int outputSize)
{
	NI=inputSize;
	NL=hiddenLayerSize;
	NO=outputSize;

	IW.resize(NI,NL);
	Ib.resize(NL,1);
	LW.resize(NL,NO);
	Lb.resize(NO,1);

	Iscl.resize(NI, 1);
	Ioff.resize(NI, 1);
	Oscl.resize(NO, 1);
	Ooff.resize(NO, 1);
}

/* read parameters */
void
MLP_feedforward :: readParam(string filename){
	ifstream f(filename);
	int m, n;

	// IW
	f >> m >> n;
	NL=m; NI=n;
	cout << "IW: " << m<<", "<<n<<endl;
	IW.resize(NL,NI);
	for(int i=0; i<m; i++){
		for(int j=0; j<n; j++){
			f >> IW(i,j);
		}
	}
	

	// Ib
	f >> m >> n;
	cout << "Ib: " << m<<", "<<n<<endl;
	Ib.resize(NL,1);
	for(int i=0; i<m; i++){
		f >> Ib(i,0);
	}
	//std::cout.precision (20) ;
	cout << Ib << endl;
	// LW
	f >> m >> n;
	cout << "LW: " << m<<", "<<n<<endl;
	NO = m;
	LW.resize(NO,NL);
	for(int i=0; i<m; i++){
		for(int j=0; j<n; j++){
			f >> LW(i,j);
		}
	}

	// Lb
	cout << "Lb: " << m<<", "<<n<<endl;
	f >> m >> n;
	Lb.resize(NO,1);
	for(int i=0; i<m; i++){
		f >> Lb(i,0);
	}

	// Iscl
	f >> m >> n;
	cout << "Iscl: " << m<<", "<<n<<endl;
	Iscl.resize(NI, 1);
	for(int i=0; i<m; i++){
		f >> Iscl(i,0);
	}
	cout << Iscl << endl;

	// Ioff
	f >> m >> n;
	cout << "Ioff: " << m<<", "<<n<<endl;
	Ioff.resize(NI, 1);
	for(int i=0; i<m; i++){
		f >> Ioff(i,0);
	}
	cout << Ioff << endl;

	// Oscl
	f >> m >> n;
	cout << "Oscl: " << m<<", "<<n<<endl;
	Oscl.resize(NO, 1);
	for(int i=0; i<m; i++){
		f >> Oscl(i,0);
	}
	cout << Oscl << endl;

	// Ooff
	f >> m >> n;
	cout << "Ooff: " << m<<", "<<n<<endl;
	Ooff.resize(NO, 1);
	for(int i=0; i<m; i++){
		f >> Ooff(i,0);
	}
	cout << Ooff << endl;
	// Imin
	f >> Imin; 
	cout << "Imin: " << Imin << endl;
	// Omin
	f >> Omin;
	cout << "Omin: " << Omin << endl;


}

/* scale inputs / outputs */
MatrixXd
MLP_feedforward :: mapminmax(MatrixXd x, MatrixXd gain, MatrixXd off, double ymin, bool reverse){
	MatrixXd y(x.size(),1);
	// SCALE FOR THE INPUT
	y=x;
	if(!reverse){
		y=y-off;
		y=y.cwiseProduct(gain);
		y=y.array()+ymin;
	}
	else{
		y=y.array()-ymin;
		y=y.cwiseQuotient(gain);
		y=y+off;
	}
	return y;
}
/*
function y = mapminmax_apply(x,settings_gain,settings_xoffset,settings_ymin)
  y = bsxfun(@minus,x,settings_xoffset);
  y = bsxfun(@times,y,settings_gain);
  y = bsxfun(@plus,y,settings_ymin);
end

% Map Minimum and Maximum Output Reverse-Processing Function
function x = mapminmax_reverse(y,settings_gain,settings_xoffset,settings_ymin)
  x = bsxfun(@minus,y,settings_ymin);
  x = bsxfun(@rdivide,x,settings_gain);
  x = bsxfun(@plus,x,settings_xoffset);
end
*/
/* activation function */
MatrixXd
MLP_feedforward :: tansig(MatrixXd x){
	MatrixXd y=-2*x;
	y=y.array().exp();
	y=y.array()+1;
	y=y.array().inverse();
	y=2*y.array()-1;
	return y;
}


/* run the network */
MatrixXd
MLP_feedforward :: run(MatrixXd x){
	// input
	MatrixXd xp1 = mapminmax(x, Iscl, Ioff, Imin, 0);

	// layer 1
	MatrixXd a1  = tansig(IW*xp1 + Ib);

	// layer 2
	MatrixXd a2  = LW*a1 + Lb;

	// output
	MatrixXd y   = mapminmax(a2, Oscl, Ooff, Omin, 1);

	return y;
}


