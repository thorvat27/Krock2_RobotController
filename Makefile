TARGET = robot_main

CTRL_HOME = $(shell pwd)
LIB_HOME = /home/odroid/Libraries
CXX = g++
INCLUDE_DIRS =  -I$(CTRL_HOME) -I$(CTRL_HOME)/header   \
				-I$(LIB_HOME) -I$(LIB_HOME)/DXLite-master -I$(LIB_HOME)/DXLite-master/dxsystem \
				-I$(LIB_HOME)/Comm -I$(LIB_HOME)/Comm/header \
				-I$(LIB_HOME)/Utils -I$(LIB_HOME)/Eigen \
				-I$(LIB_HOME)/casadi-master/casadi -I$(LIB_HOME)/casadi-master/casadi/build/lib/ \
				-I$(LIB_HOME)/dlib-19.0/ \
				-I$(LIB_HOME)/qpOASES-3.2.0/include/ \
				-I$(LIB_HOME)/vectornav -I$(LIB_HOME)/vectornav/header \
				-I$(LIB_HOME)/Optoforce/header 
				#-I$(LIB_HOME)/nnTools

DISABLE_WARNING_FLAGS = -Wno-unused-variable -Wno-unused-but-set-variable -Wno-strict-overflow -Wno-write-strings \
						-Wno-unused-value


CXXFLAGS += -std=c++11 -std=gnu++11	-O3 -DLINUX  -Wall -fmessage-length=0 $(INCLUDE_DIRS) $(DISABLE_WARNING_FLAGS)

#CXXFLAGS += -std=c++11	-O2 -mfpu=neon -march=native -mtune=cortex-a15 -DLINUX  -Wall -fmessage-length=0 $(INCLUDE_DIRS)
LIBS +=  $(LIB_HOME)/libdxlite.so $(LIB_HOME)/libps3comm.so $(LIB_HOME)/libvectornav.so $(LIB_HOME)/liboptoforce.so \
		-L$(LIB_HOME)/casadi-master/casadi/build/lib/ -lcasadi
		# $(LIB_HOME)/libNNkeras.so 
LIBS += -L$(LIB_HOME)/qpOASES-3.2.0/bin/ -lqpOASES
LIBS += -pthread -lrt -ljpeg -lboost_system -lboost_thread



SOURCES = \
	$(LIB_HOME)/Utils/utils.cpp \
	$(LIB_HOME)/Utils/misc_math.cpp \
	main.cpp  \
	source/controller.cpp \
	source/controller_getParameters.cpp \
	source/controller_misc.cpp \
	source/controller_inverseKinematics.cpp \
	source/controller_forces.cpp \
	source/controller_joystick.cpp \
    source/robot.cpp \
    source/mlp.cpp \
    source/controller_trajectory.cpp \
    source/controller_walking.cpp \
    source/optimization_tools.cpp \
    $(LIB_HOME)/nnTools/NeuralNet.cpp \
	$(LIB_HOME)/nnTools/keras_model.cpp 
    
    


OBJS = $(SOURCES:.cpp=.o)

all: $(SOURCES) $(TARGET)


$(TARGET): $(OBJS) 
	$(CXX) -o $(TARGET) $(OBJS)  $(LIBS) 


clean:
	rm -f $(OBJS) $(TARGET)

