#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fstream>
#include <cmath>
#include "controller.hpp"
#include "joystick.h"
#include <sched.h>
#include "robot.hpp"
#include "utils.hpp"
#include "struct_defs.hpp"



#define MAX_NUM_SERVOS 30

#define TIME_STEP           10    //ms

//#define EIGEN_DONT_ALIGN_STATICALLY

using namespace std;


// GLOBAL CONFIG
int IS_SIMULATION;
int IS_OPTIMIZATION;
int USE_JOYSTICK;
int SWIM;
int SPINE_COMPENSATION_WITH_FEEDBACK;
int USE_REFLEXES;
int USE_TAIL;
int USE_IMU;
int USED_IDS[30], ENABLED_IDS[30];
int AUTO_RUN, AUTO_RUN_STATE;
int LOG_DATA;
int JOYSTICK_RECORDING_REPLAYING;
int Ntrunk, Nlegs, Ntail, Nneck, NUM_MOTORS;
int MAX_TORQUE;
int MAX_SPEED;
int DISABLE_ALL_MOTORS;


// load global config
int readGlobalConfig();

int
main(int argc, char* argv[])
{

    cout << "STARTING MAIN" << endl;

    if(readGlobalConfig()==0){
        return 0;
    }

    for(int i=0; i<NUM_MOTORS; i++){
        cout << "motor " << i <<": " << USED_IDS[i] << "\t" <<ENABLED_IDS[i]<< "\t";
    }
    cout << endl;
    cout << "GLOBAL LOADED" << endl;
    //========== Initialize variables =================            
    double Td=TIME_STEP/1000.;
    double t=0, t_tot=0;
    double angles[MAX_NUM_SERVOS], fbck_angles[MAX_NUM_SERVOS], fbck_current[MAX_NUM_SERVOS];
    double t0=get_real_time(), t1=get_real_time(), t2=get_real_time(), dt;
    int numsteps=0;
    double senddata[155];
    double cnt_time=0;

    // ================= INITIALIZE ROBOT STATUS =====================================
    RobotStatus rStatus((const char*)"rstatus2unity_shm");
    rStatus.WriteSharedMemory(); printf("written to a shm\n");

    // ================= INITIALIZE SENSORS =====================================
    managed_shared_memory ssshm(open_or_create, "sensorsStruct_shm", 1024);
    SensorsStruct *sensorsStruct_shm;
    sensorsStruct_shm = ssshm.find_or_construct<SensorsStruct>("SensorsStruct")();

    //========= Initialize controller ==================
    cout<<"CREATING CONTROLLER OBJECT"<<endl;
    Controller controller(Td);
    cout<<"CONTROLLER INITIALIZED"<<endl;

    //========= Initialize robot ==================
    Robot robot;
    robot.initSharedMemory();
    robot.initMotorSystem(200, 100, NUM_MOTORS, USED_IDS, ENABLED_IDS, "/dev/usb2rs485");
    cout<<"ROBOT INITIALIZED"<<endl;

    usleep(50000);
    cout << "max torque: " <<MAX_TORQUE <<"\t" <<"max speed: "<< MAX_SPEED<< endl;
    robot.MaxSpeed(MAX_SPEED);
    usleep(10000);
    robot.MaxTorque(1023);
    usleep(10000);
    robot.TorqueLimit(MAX_TORQUE); 
    usleep(1000);
    

    

    double imudata[3], forcedata[12], forcedataRaw[16];
    //#################### LOOP ############################################################
    //######################################################################################
    //robot.getTorqueEnable();
    while(1)
    {  
        //=========================== Time flow ===============================
        dt=get_timestamp()-t0;
        t0=get_timestamp();
        t=t+dt;
        
        dt=dt>(TIME_STEP/1000.*2)?(TIME_STEP/1000.):dt;

        //=========================== Read sensors ===============================
        robot.readIMU(imudata);
        robot.readForceSensors(forcedata, forcedataRaw);

        //======================= Robot feedback =====================================
        robot.getAllPositionsThread(fbck_angles);
        //robot.getAllCurrentsThread(fbck_current);

        /*cout << "optoforce \t";
        for(int i=0; i<16; i++){
            cout << forcedataRaw[i] << "\t";
            if((i+1)%4==0){
                cout << "|||" << "\t";
            }
        }
        cout << endl;*/
        //=========================== Controller calls ===============================
        controller.setTimeStep(dt);
        controller.updateRobotState(fbck_angles, fbck_current);
        controller.getRPY(imudata);
        controller.getForce(forcedata, forcedataRaw);

        if(!controller.runStep()){
            //robot.robot_comm_thread.join();
            robot.stopThread=true;
            usleep(300000);
            cout << "leaving the loop" << endl;
            break;
        }
        controller.getAngles(angles);
        
        /*for(int i=16; i<18; i++){
            cout <<"||"<< i <<"=>"<< fbck_angles[i]*180/3.14159 <<"=>"<< angles[i]*180/3.14159<< "\t";
        }
        cout << endl;*/

        robot.setAllPositionsThread(angles);
        //usleep(1000);
        //robot.getAllPositions(table_ret);

        //============================= SEND ROBOT STATUS =========================================
        for(int i=0; i<NUM_MOTORS; i++){
            robot.rStatus.robotStatusStruct.motor_positions[i]=controller.joint_angles(i);
            robot.rStatus.robotStatusStruct.motor_torques[i]=controller.fbck_torques(i);
        }
        for(int i=0; i<2; i++){
            robot.rStatus.robotStatusStruct.imu[i]=controller.forRPY(i);
        }
        for(int i=0; i<12; i++){
            robot.rStatus.robotStatusStruct.forces[i]=controller.force(i);    
        }
        robot.rStatus.WriteSharedMemory();
        //============================= SEND UDP ======================================================




        if(LOG_DATA){

            static ofstream fullBodyKinematicsLog("./data/fullBodyKinematicsLog.txt");
            static ofstream timeLog("./data/timeLog.txt");

            // TIME
            timeLog<<t<<endl;
      

            // FORWARD KINEMATICS
            


        }

        controller.sendStatusToMatlab(10, "128.179.164.226");
        //=========================== Sleep until time step ===============================
        t1=get_timestamp();
        if(Td>(t1-t0)){
            usleep((Td- (t1-t0))*1000000 );
        }
        t2=get_timestamp();
        //printf("%4.5f ms \t\t %4.5f ms \t state: %d\n", (t1 - t0) * 1000, (t2 - t0) * 1000, controller.state);

        //=========================== CHECK IF TIME IS OK =====================
        if((t1 - t0)*1000 > 28){
            cnt_time++;
        }
        else{
            cnt_time=0;
        }
        if(cnt_time>50){
            cout<<"SOMETHING IS WRONG!!!!!!!!!!!!!!!!!!!\nCHECK CABLES, SOMETHING IS PROBABLY DISCONNECTED!!!!!!!!"<<endl;
            break;

        }

    


        numsteps++;


    }
    
    //#################### LOOP ENDS #######################################################
    //######################################################################################
    robot.MaxSpeed(100);
    usleep(100000);
    robot.InitialPosture(ENABLED_IDS);
    usleep(2000000);
    cout << "SHUTING DOWN" << endl;
    robot.ServosOff(ENABLED_IDS);
    //controller.mpc_thread.join();
    usleep(1000000);
    return 0;
}




// load global config
int readGlobalConfig()
{
    stringstream stringstream_file;
    ifstream global_config_file("config/GLOBAL.config");
    if(global_config_file.is_open()){
        readFileWithLineSkipping(global_config_file, stringstream_file);
        stringstream_file >> IS_SIMULATION;
        stringstream_file >> IS_OPTIMIZATION;
        stringstream_file >> USE_JOYSTICK;
        stringstream_file >> SWIM;
        stringstream_file >> SPINE_COMPENSATION_WITH_FEEDBACK;
        stringstream_file >> USE_REFLEXES;
        stringstream_file >> USE_TAIL;
        stringstream_file >> USE_IMU;
        stringstream_file >> AUTO_RUN;
        stringstream_file >> AUTO_RUN_STATE;
        stringstream_file >> LOG_DATA;
        stringstream_file >> JOYSTICK_RECORDING_REPLAYING;
        stringstream_file >> Nlegs;
        stringstream_file >> Ntrunk;
        stringstream_file >> Nneck;
        stringstream_file >> Ntail;
        NUM_MOTORS=4*Nlegs + Ntrunk + Nneck + Ntail;
        
        stringstream_file >> MAX_TORQUE;
        stringstream_file >> MAX_SPEED;

        stringstream_file >> DISABLE_ALL_MOTORS;

        // IDs
        cout << "USED_IDS: \n";
        for(int i=0; i<NUM_MOTORS; i++){
            stringstream_file >> USED_IDS[i];
            stringstream_file >> ENABLED_IDS[i];
        }
        return 1;
    }
        
    else{
        return 0;
    }
}

