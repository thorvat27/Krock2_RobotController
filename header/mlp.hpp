 #ifndef MLP_HPP
#define MLP_HPP

#include <casadi/casadi.hpp>
#include <fstream>
#include <pthread.h>
#include <vector>
#include <string.h>
#include <iostream>
#include "Eigen/Dense"
#include "Eigen/Geometry"
#include <iostream>
#include <cmath>
#include <ctime>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdlib.h>


using namespace Eigen;
using namespace std;



class MLP_feedforward{

	public:
    //================== public variables ===============================================
	

    //================== public functions ===============================================
	MLP_feedforward();
	MLP_feedforward(int inputSize, int hiddenLayerSize, int outputSize);
	MatrixXd mapminmax(MatrixXd x, MatrixXd gain, MatrixXd off, double ymin, bool reverse);
	MatrixXd tansig(MatrixXd x);
	MatrixXd run(MatrixXd x);
	void readParam(string filename);



	
    private:
    //================== private variables ===============================================
	Matrix<double, Dynamic, Dynamic> IW, LW, Ib, Lb;   // weights and biases
	Matrix<double, Dynamic, Dynamic> Iscl, Ioff, Ooff, Oscl; 	 // input / output scaling
	double Imin, Omin; // input / output scaling
	
	int NI, NL, NO;


	//================== private functions ===============================================
	



};
#endif