#ifndef ROBOT_HPP
#define ROBOT_HPP

#include "utils.hpp"
#include <unistd.h>
#include <libgen.h>

#include "vectornav.h"
#include "dxmotorsystem.h"
#include "dxsystem/setup.h"
#include "dxsystem/additionaltools.h"
#include "dxsystem.h"
#include "control_table_constants.h"
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "ps3joy.hpp"
#include "struct_defs.hpp"
#include "Eigen/Dense"
#include "Eigen/Geometry"
#include "Eigen/SVD"

#include <thread>  
#include <mutex> 

#define IS_RADIAN true
#define IS_DEGREE false
#define ANGLE_TYPE IS_RADIAN

using namespace boost::interprocess;
using namespace Eigen;

/* Configuration and numbering */
#define MAX_NUM_SERVOS  30

extern int IS_SIMULATION, WHICH_ROBOT, USE_JOYSTICK, JOYSTICK_TYPE, SWIM;
extern int SPINE_COMPENSATION_WITH_FEEDBACK, USE_REFLEXES, USE_TAIL, USED_MOTORS[30], USED_IDS[30], USE_IMU, AUTO_RUN, AUTO_RUN_STATE, LOG_DATA, NUM_MOTORS;
extern int DISABLE_ALL_MOTORS;

enum{HEAD, SP1, SP2, SP3, SP4, SP5, SP6, SP7, SP8, SP9, SP10,
    LF_YAW, LF_PITCH, LF_ROLL, LF_ELBOW,
    RF_YAW, RF_PITCH, RF_ROLL, RF_ELBOW,
    LH_YAW, LH_PITCH, LH_ROLL, LH_KNEE,
    RH_YAW, RH_PITCH, RH_ROLL, RH_KNEE};

/* Robot Class */
class Robot
{
    public:
        DXMotorSystem d{0,1, "somethingRandom"};
        int numMotors;

        /* Motors' centers */
        double d_center[MAX_NUM_SERVOS];
        double d_posture[MAX_NUM_SERVOS];
        double d_speed[MAX_NUM_SERVOS];


        std::thread robot_comm_thread;

        vector<int> usedIDs;
        int ids_arr[MAX_NUM_SERVOS];

        double fbck_angles[MAX_NUM_SERVOS];
        double fbck_current[MAX_NUM_SERVOS];
        double angles[MAX_NUM_SERVOS];

        bool readPosition, readCurrent, writeAngles, stopThread;

        // shared memories
        RobotStatus rStatus{(const char*)"rstatus2unity_shm"};
        managed_shared_memory ssshm{open_or_create, "sensorsStruct_shm", 1024};
        SensorsStruct *sensorsStruct_shm, sensorsStruct;
        
         

        //Robot(int, int, int*);
        Robot(int speed, int torque, int numMotorsConfig, int used_ids[30], int enabled_ids[30], const char *portName);
        Robot();
        ~Robot();

        bool initMotorSystem(int speed, int torque, int numMotorsConfig, int used_ids[30], int enabled_ids[30], const char *portName);
        void initSharedMemory();

        void readIMU(double *imudata);
        void readForceSensors(double *forcedata, double *forcedataRaw);
        void writeRobotStatus(MatrixXd joint_angles, MatrixXd fbck_torques, MatrixXd forRPY, MatrixXd force);

        void setSinglePosition(int, double);
        void setSingleTorque(int, int);
        void setAllPositions(double* angles, const vector<int>& ids);
        void setAllPositionsThread(double* new_angles);
        void setAllTorques(double *torques, const vector<int>& ids);
        void setTorqueControl(int *torque_ctrl_on, const vector<int>& ids);
        void InitialPosture(int*);
        void MaxTorque(int);
        void TorqueLimit(int torque);
        void MaxSpeed(int);
        void TorqueMode(int*, int*);
        void ServosOn(int*);
        void ServosOff(int*);
        void setAllDelay(int val);
        int getSinglePosition(int);
        void getAllPositions(double *angles);
        void getAllPositionsThread(double *angles);
        void getIDsPositions(double *angles, const vector<int>& usedIDs);
        void getTorqueEnable();
        void setAllGainP(int Pgain);
        int translateTorque(double torque);
        void getAllCurrents(double *currents);
        void getAllCurrentsThread(double *currents);
    
};

#endif

















































